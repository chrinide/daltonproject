import os
import runpy
from shutil import copyfile

from daltonproject.utilities import chdir


def test_casscf_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/pyridine.mol'), 'pyridine.mol')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/simple_cas.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2_no_inspection.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2no_cas.py'))


def test_spectrum_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/water.xyz'), 'water.xyz')
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/acrolein.xyz'),
                 'acrolein.xyz')
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/H2O2.xyz'), 'H2O2.xyz')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/simple_tddft.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/simple_tpa.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/cvs_cc.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/ir_raman.py'))
