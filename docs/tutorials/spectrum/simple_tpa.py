import matplotlib.pyplot as plt

import daltonproject as dp

molecule = dp.Molecule(input_file='water.xyz')
basis = dp.Basis(basis='cc-pVDZ')

dft = dp.QCMethod('DFT', 'CAMB3LYP')
prop = dp.Property(two_photon_absorption=True)
prop.two_photon_absorption(states=3)
result = dp.dalton.compute(molecule, basis, dft, prop)
print('Excitation energies =', result.excitation_energies)
# Excitation energies = [ 7.70898022 10.45777647 11.63157226]

print('Two-photon cross-sections =', result.two_photon_cross_sections)
# Two-photon cross-sections = [0.139 0.776 0.585]

ax = dp.spectrum.plot_two_photon_spectrum(result, color='k')
plt.savefig('2pa.svg')

if True:
    import numpy as np
assert np.allclose(result.excitation_energies, [7.70898022, 10.45777647, 11.63157226])
assert np.allclose(result.two_photon_cross_sections, [0.139, 0.776, 0.585])
